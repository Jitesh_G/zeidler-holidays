var months = { '1': [], '2': [], '3': [], '4': [], '5': [], '6': [], '7': [], '8': [], '9': [], '10': [], '11': [], '12': [] };

$("#selectCountry, #selectYear").change(function (e) {
  $("#output").html("Loading...");
  var country = $("#selectCountry").val();
  var year = $("#selectYear").val();
  var api = 'https://holidayapi.pl/v1/holidays?country=' + country + '&year=' + year;
  var mlist = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

  $.get(api, function (data) {
    var holidayDates = data.holidays;
    for (var holidayDate in holidayDates) {
      var mth = parseInt(holidayDate.split("-")[1]);
      for (var hdata of holidayDates[holidayDate]) {
        months[mth].push(hdata.name);
      }
    }
    var htm = "";
    htm += `<div class="list-of-holidays">`;
    for (var m in months) {
      htm += `<div><h3>${mlist[m - 1]}</h3>`;
      for (var h in months[m]) {
        htm += `<p>${months[m][h]}</p>`;
      }
      htm += `</div>`;
    }
    htm += "</div>";
    $("#output").html(htm);

  });

});